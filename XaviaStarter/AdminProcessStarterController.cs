﻿using System;
using System.Diagnostics;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using System.Windows;
using System.Drawing;
using IWshRuntimeLibrary;
using File = System.IO.File;
using TsudaKageyu;

namespace AdminProcessStarter
{
    public class AdminProcessStarterController : IAdminProcessStarterController
    { 
        public bool SaveData(string username, SecureString securePassword, string applicationPath, string shortcutName)
        {
            if (!ValidateApplicationExists(applicationPath))
            {
                throw new AdminProcessStarterException("Invalid path",
                    "The given application does not exist, please verify.", MessageBoxImage.Error);
            }

            var password = ConvertToUnsecureString(securePassword);
            if (!ValidateCredentials(username, password))
            {
                throw new AdminProcessStarterException("Invalid Credentials", 
                    "Username or password incorrect, please try again.", MessageBoxImage.Error);
            }

            var directoryInfo = new DirectoryInfo(@"C:/Users/Public/");
            var dataFilePath = @"C:\\Usuarios\\Público\\admin-starter-data.pin";

            if (directoryInfo.Exists)
                dataFilePath = $"{directoryInfo.FullName}admin-starter-data.pin";

            GenerateDataFile(password, applicationPath, dataFilePath, username);

            GenerateShortcut(dataFilePath, shortcutName, applicationPath);

            return true;
        }

        #region Helpers

        private static bool ValidateCredentials(string username, string password)
        {
            try
            {
                var domainName = System.DirectoryServices.ActiveDirectory.Domain.GetComputerDomain().ToString();
                using (var context = new PrincipalContext(ContextType.Domain, domainName))
                {
                    if (context.ValidateCredentials(username, password))
                        return true;
                }
            }
            catch
            {
                // ignored
            }

            using (var context = new PrincipalContext(ContextType.Machine))
            {
                if (context.ValidateCredentials(username, password))
                    return true;
            }
            return false;
        }

        private static bool ValidateApplicationExists(string applicationPath)
        {
            var fileInfo = new FileInfo(applicationPath);
            return fileInfo.Exists;
        }

        private string ConvertToUnsecureString(SecureString securePassword)
        {
            if (securePassword == null)
                throw new ArgumentNullException(nameof(securePassword));

            var unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }

        private static void GenerateShortcut(string dataFilePath, string shortcutName, string applicationPath)
        {
            var applicationFileInfo = new FileInfo(applicationPath);
            var applicationDirectory = applicationFileInfo.Directory?.FullName;

            try
            {
                // copy CPAU to Working Directory
                try
                {
                    File.Copy(@"CPAU.exe", $"{applicationDirectory}\\cpau.exe");
                }
                catch
                {
                    // ignored
                }


                string shortcutPath = $"C:/Users/Public/Desktop/{shortcutName}.lnk";
                var shell = new WshShell();
                var shortcut = (WshShortcut)shell.CreateShortcut(shortcutPath);

                string arguments = $"-dec -file {dataFilePath} -lwp";
                shortcut.TargetPath = $"{applicationDirectory}\\cpau.exe";
                shortcut.Arguments = arguments;
                shortcut.IconLocation = GetAssociatedIcon(applicationPath);
                shortcut.WorkingDirectory = applicationDirectory;
                shortcut.Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static string GetAssociatedIcon(string applicationPath)
        {
            IconExtractor iconExtractor = new IconExtractor(applicationPath);
            string fileName = iconExtractor.FileName;

            Icon biggest = iconExtractor.GetIcon(0);
            for (int i = 0; i < iconExtractor.Count; i++)
            {
                Icon auxIcon = iconExtractor.GetIcon(i);
                if (auxIcon.Width > biggest.Width)
                    biggest = auxIcon;
            }
            
            FileStream fs = new FileStream($"{fileName}.ico", FileMode.Create);
            biggest.Save(fs);
            fs.Close();

            return fileName;
        }

        private static void GenerateDataFile(string password, string applicationPath, string dataFilePath, string username)
        {
            const string cpauPath = @"CPAU.exe";
            string arguments = $"-u .\\{username} -p {password} -ex \"{applicationPath}\" -enc -file \"{dataFilePath}\"";

            var processStartInfo = new ProcessStartInfo(cpauPath, arguments)
            {
                UseShellExecute = false,
                Verb = "runas",
                RedirectStandardOutput = true
            };
            var process = new Process { StartInfo = processStartInfo };
            process.Start();

            process.WaitForExit();
            Debug.Write(process.StandardOutput.ReadToEnd());

            try
            {
                File.SetAttributes(dataFilePath,
                    File.GetAttributes(dataFilePath) | FileAttributes.Hidden);
            }
            catch
            {
                Thread.Sleep(1500);

                try
                {
                    File.SetAttributes(dataFilePath,
                        File.GetAttributes(dataFilePath) | FileAttributes.Hidden);
                }
                catch
                {
                    /* ignored */
                }
            }
        }

        #endregion
    }
}
