﻿using System;
using System.Security.Policy;
using System.Windows;

namespace AdminProcessStarter
{
    public class AdminProcessStarterException : Exception
    {
        private readonly MessageBoxImage _image;

        public string Title { get; set; }

        public AdminProcessStarterException(string title, string message, MessageBoxImage image = MessageBoxImage.Information)
            : base(message)
        {
            Title = title;
            _image = image;
        }

        public void ShowMessage()
        {
            MessageBox.Show(Message, Title, MessageBoxButton.OK, _image);
        }
    }
}
