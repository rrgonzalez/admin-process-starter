﻿using System.Security;

namespace AdminProcessStarter
{
    public interface IAdminProcessStarterController : IController
    {
        bool SaveData(string username, SecureString securePassword, string applicationPath, string shortcutName);
    }
}
