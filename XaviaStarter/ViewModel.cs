﻿using System.Windows;
using System.Windows.Controls;
﻿using System.Windows.Input;
using System.Windows.Forms;
using MessageBox = System.Windows.MessageBox;

namespace AdminProcessStarter
{
    public class ViewModel : BaseViewModel
    {
        public IAdminProcessStarterController AdminProcessStarterController => (IAdminProcessStarterController) Controller;

        #region Constructor

        public ViewModel()
        {
            ShortcutName = "";
            ApplicationPath = "";
            Username = "Administrator";
            Controller = new AdminProcessStarterController();
        }

        #endregion

        #region Properties

        #region ObservableProperties

        private string _shortcutName;
        private string _applicationPath;
        private string _username;

        public string ShortcutName
        {
            get { return _shortcutName; }
            set
            {
                _shortcutName = value;
                RaisePropertyChanged("ShortcutName");
            }
        }

        public string ApplicationPath
        {
            get { return _applicationPath; }
            set
            {
                _applicationPath = value;
                RaisePropertyChanged("ApplicationPath");
            }
        }

        public string Username
        {
            get { return _username; }
            set
            {
                if (value == _username) return;
                _username = value;
                RaisePropertyChanged("Username");
            }
        }

        #endregion
        #endregion

        #region Commands

        private ICommand _saveDataCommand;

        public ICommand SaveDataCommand => _saveDataCommand ??
                                           (_saveDataCommand = new RelayCommand<object>(SaveDataExecuted));

        private ICommand _selectApplicationCommand;

        public ICommand SelectApplicationCommand => _selectApplicationCommand ??
                                                    (_selectApplicationCommand = new RelayCommand(SelectApplicationExecuted));

        #region Command Executed

        private void SaveDataExecuted(object parameter)
        {
            var securePassword = (parameter as PasswordBox)?.SecurePassword;
            try
            {
                if (AdminProcessStarterController.SaveData(Username, securePassword, ApplicationPath, ShortcutName))
                    MessageBox.Show("Starter data recorded succesfully", "Data recorded", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (AdminProcessStarterException ex)
            {
                ex.ShowMessage();
            }
        }

        private void SelectApplicationExecuted()
        {
            var openFileDialog = new OpenFileDialog
            {
                CheckFileExists = true,
                DefaultExt = ".exe",
                Multiselect = false
            };

            var result = openFileDialog.ShowDialog();

            if (result != DialogResult.OK)
                return;

            ApplicationPath = openFileDialog.FileName;
        }

        #endregion
        #endregion
    }
}
